﻿using System;
using UnityEngine;
using NodeEditorFramework.IO;
using Newtonsoft.Json;
using GenericMenu = NodeEditorFramework.Utilities.GenericMenu;
using NodeEditorFramework.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using System.Linq;

namespace NodeEditorFramework.Standard
{
    public class NodeEditorInterface
    {
        public NodeEditorUserCache canvasCache;
        public Action<GUIContent> ShowNotificationAction;

        // GUI
        public string sceneCanvasName = "";
        public float toolbarHeight = 50;

        // Modal Panel
        public static bool showModalPanel;
        public Rect modalPanelRect = new Rect(20, 50, 250, 70);
        public Action modalPanelContent;

        // IO Format modal panel
        private ImportExportFormat IOFormat;
        private object[] IOLocationArgs;
        private delegate bool? DefExportLocationGUI(string canvasName, ref object[] locationArgs);
        private delegate bool? DefImportLocationGUI(ref object[] locationArgs);
        private DefImportLocationGUI ImportLocationGUI;
        private DefExportLocationGUI ExportLocationGUI;

        private string libraryFolder =
#if UNITY_EDITOR
            Application.dataPath + "/../Lib";
#else
            Application.dataPath + "/../Library";
#endif
        private Node selectedNode;
        CustomNode node = null;
        Vector2 scrollPos, libraryScroll;
        public void ShowNotification(GUIContent message)
        {
            if (ShowNotificationAction != null)
                ShowNotificationAction(message);
        }

        #region GUI

        public void DrawToolbarGUI(Rect rect)
        {
            rect.height = toolbarHeight;
            GUILayout.BeginArea(rect, NodeEditorGUI.toolbar);
            GUILayout.BeginHorizontal();
            float curToolbarHeight = 0;

            if (GUILayout.Button("New Project", NodeEditorGUI.toolbarDropdown, GUILayout.Width(120)))
            {
                canvasCache.NewNodeCanvas(typeof(NodeCanvas));
                ExternalFunctions.CreateProject(canvasCache.nodeCanvas);
            }
            if (GUILayout.Button("Save project", NodeEditorGUI.toolbarDropdown, GUILayout.Width(120)))
            {
                ExternalFunctions.SaveProject(canvasCache.nodeCanvas);
            }

            if (GUILayout.Button("Save project as", NodeEditorGUI.toolbarDropdown, GUILayout.Width(120)))
            {
                ExternalFunctions.SaveProject(canvasCache.nodeCanvas, true);
                canvasCache.SetCanvas(ExternalFunctions.OpenProject(canvasCache.nodeCanvas, canvasCache.nodeCanvas.savePath));
            }

            if (GUILayout.Button("Open project", NodeEditorGUI.toolbarDropdown, GUILayout.Width(120)))
            {
                canvasCache.NewNodeCanvas(typeof(NodeCanvas));
                canvasCache.SetCanvas(ExternalFunctions.OpenProject(canvasCache.nodeCanvas));
            }

            curToolbarHeight = Mathf.Max(curToolbarHeight, GUILayoutUtility.GetLastRect().yMax);

            GUILayout.Space(10);
            GUILayout.FlexibleSpace();

            GUILayout.Label(new GUIContent("" + canvasCache.nodeCanvas.saveName + " (" + canvasCache.typeData.DisplayString + ")",
                                            "Opened Canvas path: " + canvasCache.nodeCanvas.savePath), NodeEditorGUI.toolbarLabel);

            GUILayout.FlexibleSpace();
            curToolbarHeight = Mathf.Max(curToolbarHeight, GUILayoutUtility.GetLastRect().yMax);

            GUI.backgroundColor = new Color(1, 0.3f, 0.3f, 1);
            //if (GUILayout.Button("Force Re-init", NodeEditorGUI.toolbarButton, GUILayout.Width(100)))
            //{
            //    NodeEditor.ReInit(true); 
            //    canvasCache.nodeCanvas.Validate();
            //}

            GUILayout.Space(20);
            if (!ExternalFunctions.InRuntime)
            {
                //Make project directory
                var folderName = Application.dataPath + "/../Projects/" + canvasCache.nodeCanvas.saveName;

                if (!Directory.Exists(folderName))
                    Directory.CreateDirectory(folderName);

                var paramsFileName = folderName + "/" + canvasCache.nodeCanvas.saveName + "_unityvalue.json";
                var vrFileName = folderName + "/" + canvasCache.nodeCanvas.saveName + "_vrvalue.json";

                if (GUILayout.Button("Update all scripts", NodeEditorGUI.toolbarButton, GUILayout.Width(100)))
                {
                    foreach (var node in canvasCache.nodeCanvas.nodes)
                    {
                        node.UpdateScript();
                    }
                }

                GUI.backgroundColor = new Color(.3f, 1, .3f, 1);

                if (canvasCache.nodeCanvas.saveName != "New Calculation Canvas" && canvasCache.nodeCanvas.nodes.Count() > 0)
                {
                    if (GUILayout.Button("Play", NodeEditorGUI.toolbarButton, GUILayout.Width(100)))
                    {
                        ExternalFunctions.InRuntime = true;
                        var nodes = canvasCache.nodeCanvas.nodes;
                        var matScript = folderName + "/" + canvasCache.nodeCanvas.saveName + ".m";
                        var matExecutable = Application.dataPath + "/../MainMatLab/main_algoritmo.m";
                        var projectNameText = Application.dataPath + "/../MainMatLab/executeProject.txt";
                        //Generate mega script
                        if (nodes != null)
                        {
                            //Create the code
                            #region MatLab Generation Code
                            var code = ExternalFunctions.GenerateGraphCode(nodes, canvasCache.nodeCanvas.saveName);
                            if (!string.IsNullOrEmpty(code))
                            {
                                using (var file = File.CreateText(matScript))
                                {
                                    file.Write(code);
                                    file.Close();
                                }
                            }
                            #endregion

                            #region Create JsonFile
                            var canvasParameters = new StringBuilder();
                            var jsonNodes = new List<MatLabNode>();
                            var vrNodes = new List<VRNode>();

                            foreach (CustomNode node in nodes)
                            {
                                var jNode = new MatLabNode
                                {
                                    Name = node.name,
                                    Parameters = node.parameters
                                };

                                var vrNode = new VRNode
                                {
                                    Name = node.name,
                                    Links = GetLinks(node),
                                    Parameters = node.parameters,
                                    PosX = node.position.x,
                                    PosY = node.position.y,
                                    //ParamLinks = node.GetParamLinks()
                                };

                                vrNodes.Add(vrNode);
                                jsonNodes.Add(jNode);
                            }

                            //Matlab Input
                            var data = ExternalFunctions.GenerateParamJsonFile(jsonNodes);
                            if (data != null)
                                canvasParameters.Append(data);

                            if (canvasParameters != null)
                            {
                                using (var file = File.CreateText(paramsFileName))
                                {
                                    file.Write(canvasParameters.ToString());
                                    file.Close();
                                }
                            }

                            //VR Input
                            var vrData = JsonConvert.SerializeObject(vrNodes);
                            if (vrData != null)
                                using (var file = File.CreateText(vrFileName))
                                {
                                    file.Write(vrData.ToString());
                                    file.Close();
                                }

                            #endregion

                            //Open Matlab script
                            if (File.Exists(matScript))
                                Process.Start(matScript);

                            //Open Matlab Executable
                            if (File.Exists(matExecutable))
                                Process.Start(matExecutable);

                            if (File.Exists(projectNameText))
                                File.Delete(projectNameText);

                            using (var file = File.CreateText(projectNameText))
                            {
                                var pojectName = new StringBuilder()
                                    .AppendLine(canvasCache.nodeCanvas.saveName);

                                file.Write(pojectName);
                                file.Close();
                            }

                            //Open VR Runtime
                            var runtime = Application.dataPath + "/../Runtime/VRuntime.exe";
                            if (File.Exists(runtime))
                                Process.Start(runtime, "-prj=" + canvasCache.nodeCanvas.saveName);
                        }
                    }
                }
            }
            else
            {
                GUI.backgroundColor = new Color(1, .8f, 0, 1);
                if (GUILayout.Button("Stop", NodeEditorGUI.toolbarButton, GUILayout.Width(100)))
                {
                    ExternalFunctions.InRuntime = false;
                    Process.GetProcessesByName("VRuntime").FirstOrDefault()?.Kill();
                }
            }
#if !UNITY_EDITOR
			GUILayout.Space(5);
            GUI.backgroundColor = new Color(1, 1, 1, 1);
			if (GUILayout.Button("Quit", NodeEditorGUI.toolbarButton, GUILayout.Width(100)))
				Application.Quit ();
#endif
            curToolbarHeight = Mathf.Max(curToolbarHeight, GUILayoutUtility.GetLastRect().yMax);
            GUI.backgroundColor = Color.white;

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
            if (Event.current.type == EventType.Repaint)
                toolbarHeight = curToolbarHeight;
        }

        private List<string> GetLinks(CustomNode node)
        {
            List<string> links = new List<string>();

            foreach (var connection in node.connectionKnobs)
                if (connection.connections.Count() > 0) //Is connected?
                {
                    foreach (var conn in connection.connections)
                    {
                        links.Add(conn.body.Title);
                    }
                }

            return links;
        }

        public void DrawConstantView(Rect rect)
        {
            //Header
            GUILayout.BeginArea(rect, NodeEditorGUI.toolbar);
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("t: ");
            canvasCache.nodeCanvas.t = FloatInput.FloatField(canvasCache.nodeCanvas.t);
            //ExternalFunctions.t = float.Parse(GUILayout.TextArea(ExternalFunctions.t.ToString()));
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("Dt: ");
            canvasCache.nodeCanvas.Dt = FloatInput.FloatField(canvasCache.nodeCanvas.Dt);
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            GUILayout.Label("nt: ");
            canvasCache.nodeCanvas.nt = FloatInput.FloatField(canvasCache.nodeCanvas.nt);
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }

        public void DrawInspector(Rect rect)
        {
            rect.y = toolbarHeight;
            //Header
            GUILayout.BeginArea(rect, NodeEditorGUI.toolbar);
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Box("Inspector", NodeEditorGUI.nodeLabelBold);
            RTEditorGUI.Seperator();
            if (NodeEditor.curEditorState?.selectedNode != null)
            {
                node = NodeEditor.curEditorState.selectedNode as CustomNode;
            }

            if (node != null)
            {
                GUILayout.Box("Node: " + node.name, NodeEditorGUI.nodeBoxBold);
                GUILayout.Label("Node Color: ");
                node.mainColor = ExternalFunctions.ColorPicker(new Rect(0, 0, 200, 200), node.mainColor);
                RTEditorGUI.Seperator();
                //Attributes list
                var connections = node.dynamicConnectionPorts;
                scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Height(rect.height - 200));
                for (int i = 0; i < connections.Count; i++)
                {
                    if (!ExternalFunctions.InRuntime)
                    {
                        GUILayout.BeginHorizontal();
                        node.parameters[i].Name = GUILayout.TextField(node.parameters[i].Name);
                        GUILayout.Label(node.parameters[i].Type.ToString());
                        if (GUILayout.Button("Update Name"))
                        {
                            if (ExternalFunctions.CheckUniqueParamName(node.parameters[i].Name))
                            {
                                var dyn = node.dynamicConnectionPorts[i];
                                dyn.name = node.parameters[i].Name;
                            }
                        }

                        if (GUILayout.Button("x", GUILayout.ExpandWidth(false)))
                        {
                            node.parameters.RemoveAt(i);
                            ExternalFunctions.RemoveUniqueParamName(connections[i].name);
                            node.DeleteConnectionPort(i);
                            i--;
                        }
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Description: ", GUILayout.ExpandWidth(false));
                        node.parameters[i].Description = GUILayout.TextField(node.parameters[i].Description);
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Default value: ", GUILayout.ExpandWidth(false));
                        node.parameters[i].DefaultValue = FloatInput.FloatField(node.parameters[i].DefaultValue);
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Delta value: ", GUILayout.ExpandWidth(false));
                        node.parameters[i].DeltaValue = FloatInput.FloatField(node.parameters[i].DeltaValue);
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Unit of measure: ", GUILayout.ExpandWidth(false));
                        node.parameters[i].UnitOfMeasure = GUILayout.TextField(node.parameters[i].UnitOfMeasure);
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Chemical composition: ", GUILayout.ExpandWidth(false));
                        node.parameters[i].ChemicalComposition = GUILayout.TextField(node.parameters[i].ChemicalComposition);
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Physical state: ", GUILayout.ExpandWidth(false));
                        node.parameters[i].PhysicalState = GUILayout.TextField(node.parameters[i].PhysicalState);
                        GUILayout.EndHorizontal();

                        RTEditorGUI.Seperator();
                    }
                    else
                    {
                        GUILayout.BeginHorizontal();

                        GUILayout.Label(node.parameters[i].Name);
                        GUILayout.Label(node.parameters[i].Type.ToString());
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Description: " + node.parameters[i].Description, GUILayout.ExpandWidth(false));
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Default value: " + node.parameters[i].DefaultValue, GUILayout.ExpandWidth(false));
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Unit of measure: " + node.parameters[i].UnitOfMeasure, GUILayout.ExpandWidth(false));
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Chemical composition: " + node.parameters[i].ChemicalComposition, GUILayout.ExpandWidth(false));
                        GUILayout.EndHorizontal();

                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Physical state: " + node.parameters[i].PhysicalState, GUILayout.ExpandWidth(false));
                        GUILayout.EndHorizontal();

                        RTEditorGUI.Seperator();
                    }
                }
                GUILayout.EndScrollView();
            }

            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        public void DrawLibrary(Rect rect)
        {
            rect.y = toolbarHeight;
            //Header
            GUILayout.BeginArea(rect, NodeEditorGUI.toolbar);
            GUILayout.BeginHorizontal();
            GUILayout.BeginVertical();
            GUILayout.Box("Library", NodeEditorGUI.nodeLabelBold);
            RTEditorGUI.Seperator();
            libraryScroll = GUILayout.BeginScrollView(libraryScroll, GUILayout.Height(rect.height - 200));
            if (Directory.Exists(libraryFolder))
            {
                var files = GetFiles(libraryFolder).Select(s => s.Replace(libraryFolder + "\\", string.Empty)).ToList();

                List<string> nodes = new List<string>();

                foreach (var f in files)
                {
                    string toAdd = f;
                    if (f.Split('\\').Count() <= 2)
                    {
                        toAdd = "Generics\\" + f;
                    }

                    nodes.Add(toAdd);
                }

                var groups = nodes.GroupBy(s => s.Split('\\').FirstOrDefault()).ToList();

                foreach (var category in groups)
                {
                    GUILayout.Label(category.Key, NodeEditorGUI.nodeLabelBold);

                    foreach (var f in category)
                    {
                        var label = f.Replace(category.Key + "\\", string.Empty);

                        var path = label.Replace(label.Split('\\').Last(), string.Empty);

                        if (GUILayout.Button(path.Substring(0, path.Length - 1), GUILayout.Height(30)))
                        {
                            CustomNode n = (CustomNode)Node.Create("customFunction", NodeEditor.ScreenToCanvasSpace(new Vector2(Screen.width / 2, Screen.height / 2)));

                            var json = libraryFolder + "\\" + f.Replace("Generics\\", string.Empty);
                            n.ReadJson(json);
                        }
                    }
                }
            }
            GUILayout.EndScrollView();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndArea();
        }

        List<string> GetFiles(string dir)
        {
            var files = new List<string>();
            foreach (var d in Directory.GetDirectories(dir))
            {
                foreach (var f in Directory.GetFiles(d).Where(fi => fi.EndsWith(".json")))
                {
                    files.Add(f);
                }
                files.AddRange(GetFiles(d));
            }
            return files;
        }

        private void SaveSceneCanvasPanel()
        {
            GUILayout.Label("Save Canvas To Scene");

            GUILayout.BeginHorizontal();
            sceneCanvasName = GUILayout.TextField(sceneCanvasName, GUILayout.ExpandWidth(true));
            bool overwrite = NodeEditorSaveManager.HasSceneSave(sceneCanvasName);
            if (overwrite)
                GUILayout.Label(new GUIContent("!!!", "A canvas with the specified name already exists. It will be overwritten!"), GUILayout.ExpandWidth(false));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Cancel"))
                showModalPanel = false;
            if (GUILayout.Button(new GUIContent(overwrite ? "Overwrite" : "Save", "Save the canvas to the Scene")))
            {
                showModalPanel = false;
                if (!string.IsNullOrEmpty(sceneCanvasName))
                    canvasCache.SaveSceneNodeCanvas(sceneCanvasName);
            }
            GUILayout.EndHorizontal();
        }

        public void DrawModalPanel()
        {
            if (showModalPanel)
            {
                if (modalPanelContent == null)
                    return;
                GUILayout.BeginArea(modalPanelRect, NodeEditorGUI.nodeBox);
                modalPanelContent.Invoke();
                GUILayout.EndArea();
            }
        }

        #endregion

        #region Menu Callbacks

        private void NewNodeCanvas(Type canvasType)
        {
            canvasCache.NewNodeCanvas(canvasType);
        }

#if UNITY_EDITOR
        //private void LoadCanvas()
        //{
        //    string path = UnityEditor.EditorUtility.OpenFilePanel("Load Node Canvas", NodeEditor.editorPath + "Resources/Saves/", "asset");
        //    if (!path.Contains(Application.dataPath))
        //    {
        //        if (!string.IsNullOrEmpty(path))
        //            ShowNotification(new GUIContent("You should select an asset inside your project folder!"));
        //    }
        //    else
        //        canvasCache.LoadNodeCanvas(path);
        //}

        //private void ReloadCanvas()
        //{
        //    string path = canvasCache.nodeCanvas.savePath;
        //    if (!string.IsNullOrEmpty(path))
        //    {
        //        if (path.StartsWith("SCENE/"))
        //            canvasCache.LoadSceneNodeCanvas(path.Substring(6));
        //        else
        //            canvasCache.LoadNodeCanvas(path);
        //        ShowNotification(new GUIContent("Canvas Reloaded!"));
        //    }
        //    else
        //        ShowNotification(new GUIContent("Cannot reload canvas as it has not been saved yet!"));
        //}

        //private void SaveCanvas()
        //{
        //    string path = canvasCache.nodeCanvas.savePath;
        //    if (!string.IsNullOrEmpty(path))
        //    {
        //        if (path.StartsWith("SCENE/"))
        //            canvasCache.SaveSceneNodeCanvas(path.Substring(6));
        //        else
        //            canvasCache.SaveNodeCanvas(path);
        //        ShowNotification(new GUIContent("Canvas Saved!"));
        //    }
        //    else
        //        ShowNotification(new GUIContent("No save location found. Use 'Save As'!"));
        //}

        //private void SaveCanvasAs()
        //{
        //    string panelPath = NodeEditor.editorPath + "Resources/Saves/";
        //    string panelFileName = "Node Canvas";
        //    if (canvasCache.nodeCanvas != null && !string.IsNullOrEmpty(canvasCache.nodeCanvas.savePath))
        //    {
        //        panelPath = canvasCache.nodeCanvas.savePath;
        //        string savedFileName = System.IO.Path.GetFileNameWithoutExtension(panelPath);
        //        if (!string.IsNullOrEmpty(savedFileName))
        //        {
        //            panelPath = panelPath.Substring(0, panelPath.LastIndexOf(savedFileName));
        //            panelFileName = savedFileName;
        //        }
        //    }
        //    string path = UnityEditor.EditorUtility.SaveFilePanelInProject("Save Node Canvas", panelFileName, "asset", "", panelPath);
        //    if (!string.IsNullOrEmpty(path))
        //        canvasCache.SaveNodeCanvas(path);
        //}
#endif

        private void LoadSceneCanvasCallback(object canvas)
        {
            canvasCache.LoadSceneNodeCanvas((string)canvas);
            sceneCanvasName = canvasCache.nodeCanvas.name;
        }

        private void SaveSceneCanvasCallback()
        {
            modalPanelContent = SaveSceneCanvasPanel;
            showModalPanel = true;
        }

        private void ImportCanvasCallback(string formatID)
        {
            IOFormat = ImportExportManager.ParseFormat(formatID);
            if (IOFormat.RequiresLocationGUI)
            {
                ImportLocationGUI = IOFormat.ImportLocationArgsGUI;
                modalPanelContent = ImportCanvasGUI;
                showModalPanel = true;
            }
            else if (IOFormat.ImportLocationArgsSelection(out IOLocationArgs))
                canvasCache.SetCanvas(ImportExportManager.ImportCanvas(IOFormat, IOLocationArgs));
        }

        private void ImportCanvasGUI()
        {
            if (ImportLocationGUI != null)
            {
                bool? state = ImportLocationGUI(ref IOLocationArgs);
                if (state == null)
                    return;

                if (state == true)
                    canvasCache.SetCanvas(ImportExportManager.ImportCanvas(IOFormat, IOLocationArgs));

                ImportLocationGUI = null;
                modalPanelContent = null;
                showModalPanel = false;
            }
            else
                showModalPanel = false;
        }

        private void ExportCanvasCallback(string formatID)
        {
            IOFormat = ImportExportManager.ParseFormat(formatID);
            if (IOFormat.RequiresLocationGUI)
            {
                ExportLocationGUI = IOFormat.ExportLocationArgsGUI;
                modalPanelContent = ExportCanvasGUI;
                showModalPanel = true;
            }
            else if (IOFormat.ExportLocationArgsSelection(canvasCache.nodeCanvas.saveName, out IOLocationArgs))
                ImportExportManager.ExportCanvas(canvasCache.nodeCanvas, IOFormat, IOLocationArgs);
        }

        private void ExportCanvasGUI()
        {
            if (ExportLocationGUI != null)
            {
                bool? state = ExportLocationGUI(canvasCache.nodeCanvas.saveName, ref IOLocationArgs);
                if (state == null)
                    return;

                if (state == true)
                    ImportExportManager.ExportCanvas(canvasCache.nodeCanvas, IOFormat, IOLocationArgs);

                ImportLocationGUI = null;
                modalPanelContent = null;
                showModalPanel = false;
            }
            else
                showModalPanel = false;
        }

        #endregion
    }
}

public class SerializedCanvas
{
    public float t { get; set; }
    public float Dt { get; set; }
    public float nt { get; set; }

    public List<MatLabNode> Nodes { get; set; }
}

public class MatLabNode
{
    public string Name { get; set; }
    public List<Parameter> Parameters { get; set; }
}

public class VRNode
{
    public string Name { get; set; }
    public List<string> Links { get; set; }
    public float PosX { get; set; }
    public float PosY { get; set; }
    public List<string> ParamLinks { get; set; }
    public List<Parameter> Parameters { get; set; }
}

public class JsonNode
{
    public string Name { get; set; }
    public int IconId { get; set; }
    public string FilePath { get; set; }
    public List<Parameter> Parameters { get; set; }
    public Vector4 MainColor { get; set; }
}