﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using NodeEditorFramework;
using System.IO;
using System.Linq;
using NodeEditorFramework.Standard;

public class RuntimeChecks : MonoBehaviour
{
    List<CustomNode> canvasNodes;

    private void Start()
    {
        //var projectPath = PlayerPrefs.GetString("LastProject");
        //Debug.Log(projectPath);
        //var lastProject = ExternalFunctions.LoadProject(projectPath);
        //ExternalFunctions.canvas = lastProject;
    }

    private void OnApplicationQuit()
    {
        //if (!string.IsNullOrEmpty(ExternalFunctions.canvas.savePath) && File.Exists(ExternalFunctions.canvas.savePath))
        //{
        //    ExternalFunctions.SaveProject(ExternalFunctions.canvas);
        //    PlayerPrefs.SetString("LastProject", ExternalFunctions.canvas.savePath);
        //}
    }

    private void Update()
    {
        if (ExternalFunctions.InRuntime)
        {
            StartCoroutine(CheckJsonFile());
        }
    }

    IEnumerator CheckJsonFile()
    {
        yield return new WaitForSeconds(2);

        if (canvasNodes == null || canvasNodes.Count() == 0)
            canvasNodes = ExternalFunctions.canvas.nodes;

        var folder = Application.dataPath + "/../Projects/" + ExternalFunctions.canvas.saveName;
        var matNodes = folder + "/" + ExternalFunctions.canvas.saveName + "_matlaboutputvalue.json";
        var vrNodes = folder + "/" + ExternalFunctions.canvas.saveName + "_vroutputvalue.json";
        var unityNodes = folder + "/" + ExternalFunctions.canvas.saveName + "_unityvalue.json";

        if (File.Exists(matNodes))
        {
            var text = File.ReadAllText(matNodes);
            var canvasData = JsonConvert.DeserializeObject<SerializedCanvas>(text);

            foreach (var jsonNode in canvasData.Nodes)
            {
                CustomNode node = canvasNodes.FirstOrDefault(n => n.name == jsonNode.Name) as CustomNode;
                node.parameters = jsonNode.Parameters;
            }

            ExternalFunctions.canvas.t = canvasData.t;
            ExternalFunctions.canvas.Dt = canvasData.Dt;
            ExternalFunctions.canvas.nt = canvasData.nt;

            using (var file = File.CreateText(unityNodes))
            {
                file.Write(File.ReadAllText(matNodes));
                file.Close();
            }

            File.Delete(matNodes);
        }

        if (File.Exists(vrNodes))
        {
            var text = File.ReadAllText(matNodes);
            var canvasData = JsonConvert.DeserializeObject<SerializedCanvas>(text);

            foreach (var jsonNode in canvasData.Nodes)
            {
                CustomNode cNode = canvasNodes.FirstOrDefault(n => n.name == jsonNode.Name) as CustomNode;
                cNode.parameters = jsonNode.Parameters;
            }
        }


    }
}
