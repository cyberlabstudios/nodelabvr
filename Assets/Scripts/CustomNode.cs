using Newtonsoft.Json;
using NodeEditorFramework;
using NodeEditorFramework.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using Debug = UnityEngine.Debug;

[Serializable]
[Node(false, "Node/Custom Function")]
public class CustomNode : Node
{
    #region Node Properties
    public const string ID = "customFunction";
    public override string GetID => ID;
    public override string Title => name;
    public override Vector2 MinSize => new Vector2(200, 10);
    public override bool AutoLayout => true;
    #endregion

    public List<Parameter> parameters = new List<Parameter>();
    public string FilePath = string.Empty;
    public Color nodeColor = Color.white;
    public int selectedIcon = 0;

    private Sprite[] icons;
    private string newConnection = string.Empty;
    private string nodeName = string.Empty;
    private int value = 0;
    private string unit = string.Empty;
    private ParamType type = ParamType.Input;
    private List<Texture2D> iconsTx = new List<Texture2D>();
    //Connections Attributes
    private ValueConnectionKnobAttribute SettingsConnections = new ValueConnectionKnobAttribute("Output", Direction.Out, "Float", ConnectionCount.Single);

    private void Awake()
    {
        if (icons == null || icons.Length == 0)
        {
            icons = Resources.LoadAll<Sprite>("Icons");
            foreach (var icon in icons)
                iconsTx.Add(ExternalFunctions.GenerateTextureFromSprite(icon));
        }
    }

    public override void NodeGUI()
    {
        GUILayout.BeginHorizontal();
        if (!ExternalFunctions.InRuntime)
        {
            GUILayout.BeginVertical();
            nodeName = RTEditorGUI.TextField(nodeName);
            if (GUILayout.Button("Update node name"))
            {
                if (ExternalFunctions.CheckUniqueNodeName(nodeName))
                {
                    ExternalFunctions.RemoveUniqueNodeName(name);

                    FilePath = string.Empty;

                    name = nodeName;

                    var folderPath = Application.dataPath + "\\..\\Projects\\" + canvas.saveName + "\\Nodes";

                    FilePath = folderPath + "\\" + name + ".m";
                }
                else
                {
                    Debug.Log("A node with name: " + nodeName + " already exists!");
                }
            }

            GUILayout.EndVertical();
            if (icons.Length > 0 && GUILayout.Button(iconsTx[selectedIcon], GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(false), GUILayout.Width(50), GUILayout.Height(50)))
            {
                selectedIcon++;
                if (selectedIcon >= icons.Length)
                    selectedIcon = 0;
            }
        }
        else
        {
            GUILayout.Box(iconsTx[selectedIcon], GUILayout.Width(50), GUILayout.Height(50));
        }

        GUILayout.EndHorizontal();

        if (dynamicConnectionPorts.Count != parameters.Count)
        {
            //Delete Old
            while (dynamicConnectionPorts.Count > parameters.Count)
                DeleteConnectionPort(dynamicConnectionPorts.Count - 1);

            //Create New
            foreach (var connect in dynamicConnectionPorts)
            {
                SettingsConnections.Direction = connect.direction;
                SettingsConnections.NodeSide = connect.direction == Direction.In ? NodeSide.Left : NodeSide.Right;
                SettingsConnections.MaxConnectionCount = connect.direction == Direction.Out ? ConnectionCount.Multi : ConnectionCount.Single;
                CreateValueConnectionKnob(SettingsConnections);
            }
        }

        GUILayout.Space(20);


        if (dynamicConnectionPorts.Any(d => d.direction != Direction.None))
        {
            for (int i = 0; i < parameters.Count; i++)
            {
                if (dynamicConnectionPorts[i].direction != Direction.None)
                {
                    GUILayout.BeginHorizontal();
                    //Draw input/output parameters
                    var style = NodeEditorGUI.nodeLabelLeft;
                    if (dynamicConnectionPorts[i].direction == Direction.Out)
                        style = NodeEditorGUI.nodeLabelRight;

                    GUILayout.Label($"{parameters[i].Name} = {parameters[i].DefaultValue} {parameters[i].UnitOfMeasure}", style);
                    ((ValueConnectionKnob)dynamicConnectionPorts[i]).SetPosition();
                    try
                    {
                        dynamicConnectionPorts[i].body = this;
                        dynamicConnectionPorts[i].Init(this, parameters[i].Name);
                        dynamicConnectionPorts[i].Validate(this);
                    }
                    catch (Exception ex)
                    {
                        Debug.Log(ex);
                    }
                    GUILayout.EndHorizontal();
                }
            }
        }

        GUILayout.Label("Work parameters:");
        if (dynamicConnectionPorts.Any(d => d.direction == Direction.None))
        {
            for (int i = 0; i < parameters.Count; i++)
            {
                if (dynamicConnectionPorts[i].direction == Direction.None)
                {
                    GUILayout.BeginHorizontal();
                    //Draw work parameters
                    GUILayout.Label($"{parameters[i].Name} = {parameters[i].DefaultValue} {parameters[i].UnitOfMeasure}", NodeEditorGUI.nodeLabelCentered);
                    ((ValueConnectionKnob)dynamicConnectionPorts[i]).SetPosition();
                    GUILayout.EndHorizontal();
                }
            }
        }
        GUILayout.Space(20);

        GUILayout.BeginHorizontal();

        if (!ExternalFunctions.InRuntime)
        {
            GUILayout.BeginVertical();

            GUILayout.Label("Param name: ");
            newConnection = RTEditorGUI.TextField(GUIContent.none, newConnection);

            GUILayout.Label("Param type: ");
            if (GUILayout.Button(type.ToString()))
            {
                type++;
                if ((int)type > (int)ParamType.WorkParameter)
                    type = ParamType.Input;
            }

            if (!string.IsNullOrEmpty(newConnection) && GUILayout.Button("Add", GUILayout.ExpandWidth(false)))
            {
                if (ExternalFunctions.CheckUniqueParamName(newConnection))
                {
                    var newParam = new Parameter(newConnection, type, value, unit);
                    parameters.Add(newParam);

                    SettingsConnections.Name = newConnection;
                    SettingsConnections.Direction = GetDirection(type);
                    SettingsConnections.NodeSide = GetSide(type);
                    SettingsConnections.MaxConnectionCount = type == ParamType.Output ? ConnectionCount.Multi : ConnectionCount.Single;
                    CreateValueConnectionKnob(SettingsConnections);

                    newConnection = string.Empty;
                    value = 0;
                    unit = string.Empty;
                }
                else
                {
                    Debug.Log($"Another parameter exists with name: {newConnection}");
                }
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(20);

        var inputs = dynamicConnectionPorts.Where(c => c.direction == Direction.In || c.direction == Direction.None).Select(dc => dc.name).ToList();
        var outputs = dynamicConnectionPorts.Where(c => c.direction == Direction.In).Select(dc => dc.name).ToList();

        if (!ExternalFunctions.InRuntime)
        {
            if (!string.IsNullOrEmpty(name))
            {
                if (inputs.Count() > 0 && outputs.Count() > 0)
                {
                    if ((string.IsNullOrEmpty(FilePath) || !File.Exists(FilePath)) && GUILayout.Button("Generate script"))
                    {
                        var folderPath = Application.dataPath + "\\..\\Projects\\" + canvas.saveName + "\\Nodes";

                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                            FilePath = folderPath + "\\" + name + ".m";
                        }

                        using (var file = File.CreateText(FilePath))
                        {
                            var code = ExternalFunctions.GenerateNodeCode(this);
                            file.Write(code);
                            file.Close();
                        }
                        Process.Start(FilePath);
                    }
                    else if (!string.IsNullOrEmpty(FilePath) && File.Exists(FilePath) && GUILayout.Button("Update script"))
                    {
                        UpdateScript();
                        Process.Start(FilePath);
                    }
                }
                else
                {
                    GUILayout.Label("Add a parameter to generate script!");
                }
            }
            else
                GUILayout.Label("Please insert a name to the node!");
        }
    }

    NodeSide GetSide(ParamType type)
    {
        return type == ParamType.Input ? NodeSide.Left : NodeSide.Right;
    }


    //public List<string> GetParamLinks()
    //{
    //    var paramLinks = new List<string>();



    //    return paramLinks;
    //}


    public void UpdateScript()
    {
        var allLines = File.ReadAllLines(FilePath).ToList();

        var indexOfComment = allLines.FindIndex(l => l.StartsWith("% Write"));

        var userCode = allLines.Skip(indexOfComment++);

        var result = new List<string>();

        result.AddRange(EditCode());
        result.AddRange(userCode);

        File.WriteAllLines(FilePath, result.ToArray());
    }

    Direction GetDirection(ParamType type)
    {
        switch (type)
        {
            case ParamType.Input:
                return Direction.In;
            case ParamType.Output:
                return Direction.Out;
            case ParamType.WorkParameter:
                return Direction.None;
        }

        return Direction.None;
    }

    protected internal override void OnAddConnection(ConnectionPort port, ConnectionPort connection)
    {
        if (port.direction == Direction.Out)
        {
            var thisParam = parameters.FirstOrDefault(p => p.Name == port.name);

            var destNode = connection.body as CustomNode;
            var destParam = destNode.parameters.FirstOrDefault(p => p.Name == connection.name);

            var paramToAssign = new Parameter(destParam.Name, destParam.Type, thisParam.DefaultValue, thisParam.UnitOfMeasure);

            paramToAssign.ChemicalComposition = thisParam.ChemicalComposition;
            paramToAssign.Description = thisParam.Description;
            paramToAssign.PhysicalState = thisParam.PhysicalState;

            destNode.parameters[destNode.parameters.FindIndex(i => i.Equals(destParam))] = paramToAssign;
        }
        base.OnAddConnection(port, connection);
    }

    public void ReadJson(string path)
    {
        var text = File.ReadAllText(path);
        var nodeData = JsonConvert.DeserializeObject<JsonNode>(text);

        name = nodeData.Name;
        parameters = nodeData.Parameters;
        FilePath = nodeData.FilePath;
        selectedIcon = nodeData.IconId;
        mainColor = nodeData.MainColor;
        foreach (var param in parameters)
        {
            SettingsConnections.Name = param.Name;
            SettingsConnections.Direction = GetDirection(param.Type);
            SettingsConnections.NodeSide = GetSide(param.Type);
            SettingsConnections.MaxConnectionCount = param.Type == ParamType.Output ? ConnectionCount.Multi : ConnectionCount.Single;
            var connection = CreateValueConnectionKnob(SettingsConnections);
            connection.Validate(this, true);
        }
    }

    List<string> EditCode()
    {
        var generated = ExternalFunctions.GenerateNodeCode(this).Split('\n').ToList();

        var indexOfComment = generated.FindIndex(l => l.StartsWith("% Write"));

        var toEdit = generated.Take(indexOfComment).ToList();

        return toEdit;
    }
}

[Serializable]
public enum ParamType
{
    Input, Output, WorkParameter
}

[Serializable]
public class Parameter
{
    public string Name;
    public ParamType Type;
    public string Description;
    public float DefaultValue;
    public float DeltaValue;
    public string UnitOfMeasure;
    public string ChemicalComposition;
    public string PhysicalState;

    public Parameter(string name, ParamType type, float defaultValue, string unitOfMeasure)
    {
        Name = name;
        Type = type;
        DefaultValue = defaultValue;
        ChemicalComposition = "null";
        UnitOfMeasure = unitOfMeasure ?? "null";
        Description = "null";
        PhysicalState = "null";
        DeltaValue = 1;
    }

    public Parameter()
    {

    }
}