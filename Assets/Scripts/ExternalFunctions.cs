﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NodeEditorFramework.Standard;
using System.IO;
using NodeEditorFramework;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System;
using Newtonsoft.Json;
using SFB;
using NodeEditorFramework.IO;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Debug = UnityEngine.Debug;

public class ExternalFunctions
{

    public static bool InRuntime = false;
    public static string LibraryFolder =
#if UNITY_EDITOR
        Application.dataPath + "/../Lib";
#else
        Application.dataPath + "/../Library";
#endif
    public static NodeCanvas canvas;


    static XMLImportExport xmlProcessor = new XMLImportExport();

    public static Dictionary<string, string> TemplateReplacer = new Dictionary<string, string>
    {
        { Constants.TemplateConstants.Outputs,""},
        { Constants.TemplateConstants.Functions,""},
        { Constants.TemplateConstants.Inputs,""},
        { Constants.TemplateConstants.FunctionName,""},
        { Constants.TemplateConstants.GraphName,""},
        { Constants.TemplateConstants.VarAssignment,""},
    };

    public static string GenerateNodeCode(CustomNode node)
    {
        var template = GetTemplate(TemplateType.Node);

        var regexPattern = @"\|\w*\|";
        var output = new StringBuilder();

        if (template != null)
        {
            foreach (var line in template.ToList())
            {
                var newLine = line;
                var map = RemapCodeTemplate(node);
                foreach (Match match in Regex.Matches(line, regexPattern))
                {
                    map.TryGetValue(match.Value, out var newValue);
                    newLine = newLine.Replace(match.Value, newValue ?? "");
                }
                output.AppendLine(newLine);
            }
        }

        return output.ToString();
    }

    public static string GenerateGraphCode(List<CustomNode> nodes, string canvasName)
    {
        if (nodes == null || string.IsNullOrEmpty(canvasName))
            return null;

        var template = GetTemplate(TemplateType.Graph);

        var regexPattern = @"\|\w*\|";
        var output = new StringBuilder();

        if (template != null)
        {
            foreach (var line in template.ToList())
            {
                var newLine = line;
                foreach (Match match in Regex.Matches(line, regexPattern))
                {
                    RemapGraphTemplate(nodes, canvasName).TryGetValue(match.Value, out var newValue);
                    newLine = newLine.Replace(match.Value, newValue ?? "");
                }
                output.AppendLine(newLine);
            }
        }

        return output.ToString();
    }

    internal static void CreateProject(NodeCanvas nodeCanvas)
    {
        var filePath = StandaloneFileBrowser.SaveFilePanel("Save Project", Application.dataPath + "\\..\\Projects\\", "MyProject", "nlv");
        var fileName = filePath.Split('\\').LastOrDefault();

        canvas = nodeCanvas;

        if (!string.IsNullOrEmpty(fileName))
        {
            using (var file = File.CreateText(filePath))
                file.Close();

            nodeCanvas.saveName = fileName.Replace(".nlv", string.Empty);
            nodeCanvas.savePath = filePath;
        }
    }

    internal static NodeCanvas LoadProject(string path)
    {
        if (!string.IsNullOrEmpty(path) && File.Exists(path))
        {
            canvas = xmlProcessor.Import(path);
            canvas.savePath = path;
            canvas.saveName = path.Split('\\').LastOrDefault().Replace(".nlv", string.Empty);
        }

        return canvas;
    }

    internal static NodeCanvas OpenProject(NodeCanvas nodeCanvas, string path = null)
    {
        var file = path;
        if (string.IsNullOrEmpty(path))
            file = StandaloneFileBrowser.OpenFilePanel("Open Project", Application.dataPath + "\\..\\Projects\\", "nlv", false).FirstOrDefault();

        canvas = nodeCanvas;

        if (!string.IsNullOrEmpty(file))
        {
            canvas = xmlProcessor.Import(file);
        }

        canvas.savePath = file;

        return canvas;
    }

    internal static void SaveProject(NodeCanvas nodeCanvas, bool asNew = false)
    {
        var fileName = nodeCanvas.savePath.Split('\\').LastOrDefault();
        var savePath = nodeCanvas.savePath;
        if (asNew)
        {
            var SourceFileName = nodeCanvas.savePath.Split('\\').LastOrDefault();
            var SourcePath = Application.dataPath + "\\..\\Projects\\" + nodeCanvas.saveName + "\\Nodes";

            var destinationFilePath = StandaloneFileBrowser.SaveFilePanel("Save Project", Application.dataPath + "\\..\\Projects\\", nodeCanvas.saveName, "nlv");

            var destinationFileName = destinationFilePath.Split('\\').LastOrDefault();
            var DestinationPath = destinationFilePath.Replace(destinationFileName, string.Empty);

            if (DestinationPath.Split('\\').LastOrDefault() != destinationFileName)
            {
                DestinationPath += destinationFileName.Replace(".nlv", string.Empty);
            }

            DestinationPath += "\\Nodes";

            fileName = destinationFileName;
            savePath = DestinationPath;

            if (nodeCanvas.nodes.Count > 0)
            {
                if (!Directory.Exists(DestinationPath))
                    Directory.CreateDirectory(DestinationPath);

                var nodes = new List<CustomNode>(nodeCanvas.nodes);

                foreach (var node in nodes)
                    node.FilePath = DestinationPath + "\\" + node.name + ".m";

                nodeCanvas.nodes = nodes;

                foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
                    Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

                //Copy all the files & Replaces any files with the same name
                foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                {
                    var destPath = newPath.Replace(SourcePath, DestinationPath);

                    if (File.Exists(destPath))
                        File.Delete(destPath);

                    File.Copy(newPath, destPath, true);
                }
            }
        }

        if (!string.IsNullOrEmpty(fileName))
        {
            if (File.Exists(savePath))
                File.Delete(savePath);

            var path = savePath.Replace("\\Nodes", string.Empty);

            if (path.Split('\\').LastOrDefault() != fileName)
                path += "\\" + fileName;

            xmlProcessor.Export(nodeCanvas, path);
            nodeCanvas.savePath = path;
            nodeCanvas.saveName = fileName.Replace(".nlv", string.Empty);
        }
    }

    public static void SaveNode(CustomNode node)
    {
        if (node == null || string.IsNullOrEmpty(node.name))
            return;

        if (!Directory.Exists(LibraryFolder))
            Directory.CreateDirectory(LibraryFolder);

        var nodeFolder = LibraryFolder + "/" + node.name;

        if (!Directory.Exists(nodeFolder))
            Directory.CreateDirectory(nodeFolder);

        if (!string.IsNullOrEmpty(node.FilePath) && File.Exists(node.FilePath))
        {
            string libPath = nodeFolder + "/" + node.name;
            libPath += ".m";


            if (node.FilePath != libPath) //Il percorso attuale è diverso dalla libreria?
            {
                if (File.Exists(libPath))
                    File.Delete(libPath);

                File.Copy(node.FilePath, libPath);
            } 

            var nodeJsonPath = nodeFolder + "/" + node.name + ".json";

            var nodeJson = new JsonNode
            {
                Name = node.name,
                IconId = node.selectedIcon,
                FilePath = libPath,
                Parameters = node.parameters,
                MainColor = new Vector4(node.mainColor.r, node.mainColor.g, node.mainColor.b, node.mainColor.a)
            };

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

            var json = JsonConvert.SerializeObject(nodeJson, settings);
            using (var file = File.CreateText(nodeJsonPath))
            {
                file.Write(json);
                file.Close();
            }
        }
    }

    public static Dictionary<string, string> RemapCodeTemplate(CustomNode node)
    {
        var inputs = node.parameters.Where(c => c.Type == ParamType.Input).ToList();
        var outputs = node.parameters.Where(c => c.Type == ParamType.Output).ToList();

        var toAssign = new List<Parameter>(node.parameters);
        var varAssignments = new List<string>();
        try
        {
            foreach (var param in toAssign)
            {
                var connectionKnob = node.connectionKnobs.FirstOrDefault(c => c.name == param.Name);

                if (connectionKnob == null) //Work parameters
                {
                    var val = string.IsNullOrEmpty(param.DefaultValue.ToString()) ? 0 : param.DefaultValue;
                    try
                    {
                        if (string.IsNullOrEmpty(param.UnitOfMeasure))
                            varAssignments.Add($"{param.Name} = {val};");
                        else
                            varAssignments.Add($"{param.Name} = {val}; %{param.UnitOfMeasure}");
                    }
                    catch (Exception ex)
                    {
                        Debug.Log(ex.Message);
                    }
                }
                else //Input | Output
                {
                    if (connectionKnob.connections.Count() > 0) //Is connected?
                    {
                        foreach (var conn in connectionKnob.connections)
                        {
                            try
                            {
                                if (param.Type == ParamType.Input)
                                {
                                    varAssignments.Add($"% {param.Name} = {conn.body.Title}.{conn.name}");
                                }
                                else
                                {
                                    varAssignments.Add($"% {conn.body.Title}.{conn.name} = {param.Name}");
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.Log(ex.Message);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
        }

        var mapping = TemplateReplacer;
        mapping[Constants.TemplateConstants.FunctionName] = node.name;
        mapping[Constants.TemplateConstants.Inputs] = $"{string.Join(",", inputs.Select(i => i.Name))}";
        mapping[Constants.TemplateConstants.Outputs] = $"{string.Join(",", outputs.Select(o => o.Name))}";
        mapping[Constants.TemplateConstants.VarAssignment] = $"{string.Join("\n", varAssignments)}";
        return mapping;
    }

    public static Dictionary<string, string> RemapGraphTemplate(List<CustomNode> nodes, string canvasName)
    {
        if (nodes == null || string.IsNullOrEmpty(canvasName))
            return null;

        var mapping = TemplateReplacer;
        var functionsCode = new StringBuilder();

        for (int i = 0; i < nodes.Count(); i++)
        {
            var n = nodes[i] as CustomNode;

            if (string.IsNullOrEmpty(n.FilePath))
                break;

            var code = File.ReadAllText(n.FilePath);
            functionsCode.Append(code);
        }

        mapping[Constants.TemplateConstants.GraphName] = canvasName;
        mapping[Constants.TemplateConstants.Functions] = functionsCode.ToString();

        return mapping;
    }

    public static bool CheckUniqueParamName(string value)
    {
        if (!canvas.paramNames.Contains(value))
        {
            canvas.paramNames.Add(value);
            return true;
        }
        return false;
    }

    public static void RemoveUniqueParamName(string value)
    {
        canvas.paramNames.Remove(value);
    }

    public static bool CheckUniqueNodeName(string value)
    {
        if (!canvas.nodeNames.Contains(value))
        {
            canvas.nodeNames.Add(value);
            return true;
        }
        return false;
    }

    public static void RemoveUniqueNodeName(string value)
    {
        canvas.nodeNames.Remove(value);
    }

    public static string[] GetTemplate(TemplateType template)
    {
        var folderPath = Application.dataPath + "/../Templates/";
        if (!Directory.Exists(folderPath))
            Directory.CreateDirectory(folderPath);

        var FilePath = folderPath + template.ToString() + ".tmp";
        if (File.Exists(FilePath))
        {
            var fileStream = new FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            fileStream.Dispose();
            return File.ReadAllLines(FilePath);
        }
        else
        {
            using (var f = File.CreateText(FilePath))
            {
                var text = string.Empty;

                if (template == TemplateType.Node)
                {
                    text = new StringBuilder()
                        .AppendLine($"function [{Constants.TemplateConstants.Outputs}] = {Constants.TemplateConstants.FunctionName}({Constants.TemplateConstants.Inputs})")
                        .AppendLine($"{Constants.TemplateConstants.VarAssignment}")
                        .AppendLine($"% Write your code below this comment.")
                        .AppendLine($"")
                        .AppendLine($"end").ToString();
                }
                else
                {
                    text = new StringBuilder()
                       .AppendLine($"classdef {Constants.TemplateConstants.GraphName}")
                       .AppendLine($"\tmethods (Static)")
                       .AppendLine($"{Constants.TemplateConstants.Functions}")
                       .AppendLine($"\tend")
                       .AppendLine($"end").ToString();
                }

                f.Write(text);
                f.Close();
            }

            return File.ReadAllLines(FilePath);
        }
    }

    public static Color ColorPicker(Rect rect, Color color)
    {
        //Create a blank texture.
        //Texture2D tex = new Texture2D(40, 40);

        #region Slider block
        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical("Box");
        //Sliders for rgb variables betwen 0.0 and 1.0
        GUILayout.BeginHorizontal();
        GUILayout.Label("R", GUILayout.Width(10));
        color.r = GUILayout.HorizontalSlider(color.r, 0f, 1f);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("G", GUILayout.Width(10));
        color.g = GUILayout.HorizontalSlider(color.g, 0f, 1f);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("B", GUILayout.Width(10));
        color.b = GUILayout.HorizontalSlider(color.b, 0f, 1f);
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        #endregion
        GUILayout.EndHorizontal();

        //Give color as RGB values.
        GUI.color = color;
        GUILayout.Label("Current Color = " + (int)(color.r * 255) + "|" + (int)(color.g * 255) + "|" + (int)(color.b * 255));
        GUI.color = Color.white;
        //Finally return the modified value.
        return color;
    }

    public static string GenerateParamJsonFile(List<MatLabNode> node)
    {
        var can = new SerializedCanvas
        {
            Dt = canvas.Dt,
            nt = canvas.nt,
            t = canvas.t,
            Nodes = node
        };

        JsonSerializerSettings settings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented
        };

        return JsonConvert.SerializeObject(can, settings);
    }

    public static Texture2D GenerateTextureFromSprite(Sprite aSprite)
    {
        var rect = aSprite.rect;
        var tex = new Texture2D((int)rect.width, (int)rect.height);
        var data = aSprite.texture.GetPixels((int)rect.x, (int)rect.y, (int)rect.width, (int)rect.height);
        tex.SetPixels(data);
        tex.Apply(true);
        return tex;
    }
}

public enum TemplateType
{
    Node, Graph
}

public static class Constants
{
    public static class TemplateConstants
    {
        public static string Outputs = "|outputs|";
        public static string Inputs = "|inputs|";
        public static string FunctionName = "|FunctionName|";
        public static string VarAssignment = "|VarAssignement|";
        public static string GraphName = "|GraphName|";
        public static string Functions = "|Functions|";
    }
}